Background Information
======================================

In today's world, getting data from one point to another reliably has become a key requirement in the design of communication systems. In order to acheive this, control of physical layer parameters are required to counteract the conditions of the radio link (e.g. the pathloss, the interference due to signals coming from other transmitters, the sensitivity of the receiver, the available transmitter power margin, etc.). Adaptive MCS does just this...it sacrifices throughput to ensure more reliable data transfer between 2 transceivers. MCS reduces the amount of actual user data to improve link reliability. 

Imagine a car driving on the highway...as it drives on the flat road, the car is able to cruise at high speeds without much resistance. As it approaches a hill, the driver of the car has to sacrifice its high speeds and apply more power to continue driving towards his/her destination. This is comparable to what adaptive modulation is.

Digital Modulation
--------------------

Digital Modulation is the process of encoding bits onto the amplitude, frequency, or phase of the high frequency sinusoid signal being transmitted over the air. There are many different digital modulation schemes used in today's technologies, all of which are used for different applications.

In general, anywhere from 1 to several bits are mapped to complex symbols, and the rate of symbol transmission determines the bandwidth of the transmitted signal. Since the signal bandwidth is determined by the symbol rate, having a large number of bits per symbol generally yields a higher data rate for a given signal bandwidth. However, the larger the number of bits per symbol, the greater the required received SNR for a given target BER. This is due to the distance between adjacent constellation points and the probability of a symbol crossing a decision regions threshold.

The constellation and bit mapping for BPSK and QPSK are shown in the figure below.

.. image:: pageImage/refConst.png
   :width: 600
   
Error Correction Code
------------------------

An error correction code is used to correct errors received in a message due to noisy communication channels. The idea behind coding is that the sender encodes the data with redudant information. The extra bits allow the receiver to correct for a limited number of bit errors without the need for a retransmission. Code rates are often given in the form of :math:`\frac{n}{m}` where n is the number of input bits and m is the number of output bits of the encoder.

In the implemented design, a Convolutional Encoder is used on the transmitter and a Viterbi Decoder is used on the receiver. The two coding rates used in the design are 1/2 and 2/3. The trellis structure for a rate 1/2 encoder is shown in the figure below.

In order to achieve a code rate of 2/3, a puncture pattern of [1 1 1 0] is used with the trellis structure from above. The puncture pattern takes in 4 bits (created from a 2 bit input) from the output of the encoder and punctures a bit out, resulting in a code rate of 2/3.