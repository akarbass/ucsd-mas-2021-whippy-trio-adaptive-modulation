Simulink Design
===================

Radio A Simulink design files can be downloaded :download:`here <FileDownloads/WhippyTrio_RadioA.zip>`.

Radio B Simulink design files can be downloaded :download:`here <FileDownloads/WhippyTrio_RadioB.zip>`.
   
RX FPGA Path includes:

- AGC
- RRC Filter
- Coarse Frequency Compensation
- Fine Frequency Compensation
- Timing Recovery/ Data Decoding
- Packet Header parser

Our custom function, labeled #1 in the figure below, parses the header and pulls out the Modulation ID and determines what demodulation technique to use.

.. image:: pageImage/RxParse.png
   :width: 600


1: Data is modulated according to Mod ID. and switched it to FPGA TX block

2: Header is packed with packet information and always modulated at BPSK

.. image:: pageImage/TxBits.png
   :width: 600

For QPSK Modulation even and odd data bits are paired together  

.. image:: pageImage/QPSK.png
   :width: 600

For BPSK Modulation every data bits is paired with a copy of itself  

.. image:: pageImage/BPSK.png
   :width: 600

FPGA Utilization
------------------
.. image:: pageImage/SeparateBPSKQPSK.jpeg
   :width: 600
   
.. image:: pageImage/RadioA.jpeg
   :width: 600
   
.. image:: pageImage/RadioB.jpeg
   :width: 600