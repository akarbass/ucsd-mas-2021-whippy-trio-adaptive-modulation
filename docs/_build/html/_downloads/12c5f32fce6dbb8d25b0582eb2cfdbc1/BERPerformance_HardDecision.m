close all
clear all
clc

%% Global Variables
fs = 520841;
Ts = 1/fs;
packetSize = 11.2e3;
maxPacketCount = 10e4;
markersize = 14;
linewidth = 2;

numError = 100;

constLen = 7;
trel = poly2trellis(constLen, [171 133]);

M_BPSK = 2; %BPSK Mod Order
k_BPSK = log2(M_BPSK); %Bits per Symbol
M_QPSK = 4; %QPSK Mod Order
k_QPSK = log2(M_QPSK); %Bits per Symbol

Rc_1_2 = 1/2; %Coding Rate
Rc_2_3 = 2/3; %Coding Rate

EsNodB = 0:15; %Same as MER
EsNoLin = 10.^(EsNodB/10);

%% Per Allocate BER/Error Count Arrays for Data Collection
% 1-BER, 2-ErrorCount 3-totalBits
berBPSK = zeros(3,length(EsNodB));
berQPSK = zeros(3,length(EsNodB));
berBPSK_1_2 = zeros(3,length(EsNodB));
berBPSK_2_3 = zeros(3,length(EsNodB));
berQPSK_1_2 = zeros(3,length(EsNodB));
berQPSK_2_3 = zeros(3,length(EsNodB));

%% Exact Theoretical Uncoded Expressions
BPSK_Theor = qfunc(sqrt(2*EsNoLin)); %Exact Expression (not Union Bound)
QPSK_Theor = 2*qfunc(sqrt(EsNoLin)) - (qfunc(sqrt(EsNoLin)).^2); %Exact Expression (not Union Bound)

%% QPSK SER is 2*Qfunc(sqrt(EsNo))
%% QPSK BER is QPSKSER*0.5
%% On Eb/No scale, Es/No is 2*Eb/No (2 bits per sample)
%% X-axis is Eb/No, QPSK = BPSK
%% X-axis is Es/No, QPSK = BPSK + 3dB

%% Coder/Decoder Objects
traceBackLen_1_2 = ceil(3*(constLen-1)/0.5);
traceBackLen_2_3 = ceil(3*(constLen-1)/(1/3));
puncPattern_1_2 = ones(6,1);
puncPattern_2_3 = [1 1 1 0].';

%% Error Rate
errorRateBPSK_1_2 = comm.ErrorRate();
errorRateBPSK_2_3 = comm.ErrorRate();
errorRateQPSK_1_2 = comm.ErrorRate();
errorRateQPSK_2_3 = comm.ErrorRate();
errorRateBPSK = comm.ErrorRate();
errorRateQPSK = comm.ErrorRate();

%% Scrambler/Descrambler
scrambler = comm.Scrambler(2,[1 1 1 0 1],[0 0 0 0]);
descrambler = comm.Descrambler(2,[1 1 1 0 1],[0 0 0 0]);

%% Pulse Shape Filter
txFilter = comm.RaisedCosineTransmitFilter('Shape','Square root','RolloffFactor',.5,'FilterSpanInSymbols',10,'OutputSamplesPerSymbol',2,'Gain',1);
rxFilter = comm.RaisedCosineReceiveFilter('Shape','Square root','RolloffFactor',.5,'InputSamplesPerSymbol',2,'DecimationFactor',1,'DecimationOffset',0,'Gain',1);

%% MER Object
mer = comm.MER('MinimumMEROutputPort',true, ...
    'XPercentileMEROutputPort',true,'XPercentileValue',90,...
    'SymbolCountOutputPort',true);

%% Modulator/Demodulator
BPSKMod = comm.PSKModulator('SymbolMapping','Gray','PhaseOffset',0,'ModulationOrder',M_BPSK,'BitInput',1);
QPSKMod = comm.PSKModulator('SymbolMapping','Gray','PhaseOffset',pi/4,'ModulationOrder',M_QPSK,'BitInput',1);

BPSKDemod = comm.PSKDemodulator('SymbolMapping','Gray','PhaseOffset',0,'ModulationOrder',M_BPSK,'BitOutput',1);
QPSKDemod = comm.PSKDemodulator('SymbolMapping','Gray','PhaseOffset',pi/4,'ModulationOrder',M_QPSK,'BitOutput',1);

%% Iterate thru Es/No

for i = 1:length(EsNodB)
    % Set Es/No
    EsNo = EsNoLin(i);
%     EsNo_1_2 = 10^((EsNodB(i) + 10*log10(Rc_1_2))/10);
%     EsNo_2_3 = 10^((EsNodB(i) + 10*log10(Rc_2_3))/10);
    
    sigma = sqrt(1/(2*EsNo));
%     sigma_1_2 = sqrt(1/(2*EsNo_1_2));
%     sigma_2_3 = sqrt(1/(2*EsNo_2_3));
    
    berBPSKTemp = 0;
    berQPSKTemp = 0;
    berBPSK_1_2_Temp = 0;
    berBPSK_2_3_Temp = 0;
    berQPSK_1_2_Temp = 0;
    berQPSK_2_3_Temp = 0;
    
    packetCount = 0;
    numErrorCount = 0;
    elapsedTime = 0;
    tic;
    while (numErrorCount < numError && packetCount < maxPacketCount) 
        packetCount = packetCount + 1;
        
        %% Transmitter
        % Generate Data Bits
        data = randi([0,1],packetSize,1);
        dataBPSK_1_2 = [randi([0,1],8412,1); zeros(traceBackLen_1_2,1)];
        dataBPSK_2_3 = [randi([0,1],11210,1); zeros(traceBackLen_2_3,1)];
        dataQPSK_1_2 = [randi([0,1],16860,1); zeros(traceBackLen_1_2,1)];
        dataQPSK_2_3 = [randi([0,1],22474,1); zeros(traceBackLen_2_3,1)];
        
        % Encode Data
        BPSKEnc_1_2 = randintrlv(convenc(dataBPSK_1_2,trel),4831);%,'term');
        BPSKEnc_2_3 = randintrlv(convenc(dataBPSK_2_3,trel,puncPattern_2_3),4831);%,'term');
        QPSKEnc_1_2 = randintrlv(convenc(dataQPSK_1_2,trel),4831);%,'term');
        QPSKEnc_2_3 = randintrlv(convenc(dataQPSK_2_3,trel,puncPattern_2_3),4831);%,'term');
        
        % Modulate Data
        BPSKSymTx = BPSKMod(data);
        QPSKSymTx = QPSKMod(data);
        BPSKSymTx_1_2 = sqrt(Rc_1_2)*BPSKMod(BPSKEnc_1_2);
        BPSKSymTx_2_3 = sqrt(Rc_2_3)*BPSKMod(BPSKEnc_2_3);
        QPSKSymTx_1_2 = sqrt(Rc_1_2)*QPSKMod(QPSKEnc_1_2);
        QPSKSymTx_2_3 = sqrt(Rc_2_3)*QPSKMod(QPSKEnc_2_3);
        
        %% Add AWGN
        % Generate BPSK & QPSK Noise
        % No Code
        noiseBPSK = sigma*randn(packetSize/k_BPSK,2);
        noiseBPSK = complex(noiseBPSK(:,1),noiseBPSK(:,2));
        noiseQPSK = sigma*randn(packetSize/k_QPSK,2);
        noiseQPSK = complex(noiseQPSK(:,1),noiseQPSK(:,2));
        % Code Rate = 1/2
        noiseBPSK_1_2 = sigma*randn(length(BPSKSymTx_1_2),2);
        noiseBPSK_1_2 = complex(noiseBPSK_1_2(:,1),noiseBPSK_1_2(:,2));
        noiseQPSK_1_2 = sigma*randn(length(QPSKSymTx_1_2),2);
        noiseQPSK_1_2 = complex(noiseQPSK_1_2(:,1),noiseQPSK_1_2(:,2));
        % Code Rate = 2/3
        noiseBPSK_2_3 = sigma*randn(length(BPSKSymTx_2_3),2);
        noiseBPSK_2_3 = complex(noiseBPSK_2_3(:,1),noiseBPSK_2_3(:,2));
        noiseQPSK_2_3 = sigma*randn(length(QPSKSymTx_2_3),2);
        noiseQPSK_2_3 = complex(noiseQPSK_2_3(:,1),noiseQPSK_2_3(:,2));
        
        % Add Noise
        BPSKSymRx = BPSKSymTx + noiseBPSK;
        QPSKSymRx = QPSKSymTx + noiseQPSK;
        BPSKSymRx_1_2 = BPSKSymTx_1_2 + noiseBPSK_1_2;
        QPSKSymRx_1_2 = QPSKSymTx_1_2 + noiseQPSK_1_2;
        BPSKSymRx_2_3 = BPSKSymTx_2_3 + noiseBPSK_2_3;
        QPSKSymRx_2_3 = QPSKSymTx_2_3 + noiseQPSK_2_3;
        
        %% Receiver
        BPSKDemodBit = BPSKDemod(BPSKSymRx);
        QPSKDemodBit = QPSKDemod(QPSKSymRx);
        BPSKDemod_1_2 = BPSKDemod(BPSKSymRx_1_2);
        BPSKDemod_2_3 = BPSKDemod(BPSKSymRx_2_3);
        QPSKDemod_1_2 = QPSKDemod(QPSKSymRx_1_2);
        QPSKDemod_2_3 = QPSKDemod(QPSKSymRx_2_3);       
       
        BPSKDecode_1_2 = vitdec(randdeintrlv(BPSKDemod_1_2,4831),trel,traceBackLen_1_2,'term','hard',puncPattern_1_2);
        BPSKDecode_2_3 = vitdec(randdeintrlv(BPSKDemod_2_3,4831),trel,traceBackLen_2_3,'term','hard',puncPattern_2_3);
        QPSKDecode_1_2 = vitdec(randdeintrlv(QPSKDemod_1_2,4831),trel,traceBackLen_1_2,'term','hard',puncPattern_1_2);
        QPSKDecode_2_3 = vitdec(randdeintrlv(QPSKDemod_2_3,4831),trel,traceBackLen_2_3,'term','hard',puncPattern_2_3);
        
        % Error Tracking
        berBPSKTemp = errorRateBPSK(data,BPSKDemodBit);
        berQPSKTemp = errorRateQPSK(data,QPSKDemodBit);
        berBPSK_1_2_Temp = errorRateBPSK_1_2(dataBPSK_1_2,BPSKDecode_1_2(1:length(dataBPSK_1_2)));
        berBPSK_2_3_Temp = errorRateBPSK_2_3(dataBPSK_2_3,BPSKDecode_2_3(1:length(dataBPSK_2_3)));
        berQPSK_1_2_Temp = errorRateQPSK_1_2(dataQPSK_1_2,QPSKDecode_1_2(1:length(dataQPSK_1_2)));
        berQPSK_2_3_Temp = errorRateQPSK_2_3(dataQPSK_2_3,QPSKDecode_2_3(1:length(dataQPSK_2_3)));
        
        
        numErrorArray = [berBPSKTemp(2,1), berQPSKTemp(2,1), berBPSK_1_2_Temp(2,1) ,berBPSK_2_3_Temp(2,1) ,berQPSK_1_2_Temp(2,1), berQPSK_2_3_Temp(2,1)];
        numErrorCount = min(numErrorArray);
        
    end
    
    fprintf('Iteration (%d/%d), EsNo = %.1f dB\n', i,length(EsNodB), EsNodB(i));
    fprintf('Number of Packets Sent = %.f\n', packetCount);
    if packetCount >= maxPacketCount
        fprintf('Packet count max reached.\n');
    end
    fprintf('Minimum Number of Errors Detected = %.f\n', numErrorCount);
    toc;
    fprintf('\n');
    
    berBPSK(:,i) = berBPSKTemp;
    berQPSK(:,i) = berQPSKTemp;
    berBPSK_1_2(:,i) = berBPSK_1_2_Temp;
    berBPSK_2_3(:,i) = berBPSK_2_3_Temp;
    berQPSK_1_2(:,i) = berQPSK_1_2_Temp;
    berQPSK_2_3(:,i) = berQPSK_2_3_Temp;
    
    % Reset Objects
    reset(errorRateBPSK);
    reset(errorRateQPSK);
    reset(errorRateBPSK_1_2);
    reset(errorRateBPSK_2_3);
    reset(errorRateQPSK_1_2);
    reset(errorRateQPSK_2_3);
end

f = figure('WindowState','maximized')
semilogy(EsNodB,BPSK_Theor,'r','LineWidth',linewidth,'markersize',markersize)
hold on
semilogy(EsNodB,berBPSK(1,:),'--*r','LineWidth',linewidth,'markersize',markersize)
semilogy(EsNodB,berBPSK_1_2(1,:),'--+r','LineWidth',linewidth,'markersize',markersize)
semilogy(EsNodB,berBPSK_2_3(1,:),'--or','LineWidth',linewidth,'markersize',markersize)
semilogy(EsNodB,QPSK_Theor,'k','LineWidth',linewidth,'markersize',markersize)
semilogy(EsNodB,berQPSK(1,:),'--*k','LineWidth',linewidth,'markersize',markersize)
semilogy(EsNodB,berQPSK_1_2(1,:),'--+k','LineWidth',linewidth,'markersize',markersize)
semilogy(EsNodB,berQPSK_2_3(1,:),'--ok','LineWidth',linewidth,'markersize',markersize)
hold off
grid on
grid minor
title('BER vs Es/No (dB) - Hard Decision Detection','FontSize',20)
xlabel('Es/No (dB)','FontSize',16)
ylabel('BER','FontSize',16)
l = legend({'BPSK - Theoretical', 'BPSK - Uncoded', 'BPSK 1/2', 'BPSK 2/3', 'QPSK - Theoretical', 'QPSK - Uncoded', 'QPSK 1/2', 'QPSK 2/3'},'NumColumns',2,'Location','southwest');% 
l.FontSize = 16;
saveas(f,'AdaptiveModBERPerformance_HardDecision.png')
save('HardDecisionWorkSpace.mat')