Modulation Switching Thresholds
================================
To identify the thresholds for modulation and code switching, it is important to understand the BER performance of the system. For digital communication applications, the design engineer needs to determine an acceptable bit error rate (BER) for the application. Once identified, the system needs to be characterized to determine what the minimum modulation error ratio (MER) is for a given modulation/coding combination to acheive the allowable BER.

MER Calculations
------------------
The MER is a measure used to quantify the performance of a digital radio transmitter or receiver in a communication system. It is analogous to the sigal-to-noise ratio in analog applications. A perfect signal at the transmitter or receiver will have all constellation points at the ideal locations. However, various imperfections in the implementation, such as noise, can cause the actual constellation points to deviate from the ideal locations, creating a "fuzzy" cloud.

In order to measure the MER in our system, the average symbol energy, :math:`E[symbol]`, along with the noise variance, :math:`\sigma^2`, need to be characterized/measured across each frame. The equation for MER is 

.. math::
   MER (dB) = 10 log (\frac{P_{av}}{\overline{\sigma^2}})\\
   \sigma^2 = \Delta I^2 + \Delta Q^2
   
   \Delta I = I_{Rx} - I_{REF}
   
   \Delta Q = Q_{Rx} - Q_{REF}

For PSK modulations, :math:`P_{av}` can be replaced with 1 as long as the average is taken over a large enough data set.

The BER vs MER simulation reference can be found :download:`here <FileDownloads/BERPerformance_HardDecision.m>`.

.. image:: pageImage/AdaptiveModBERPerformance_HardDecisionSlides.png
   :width: 1200
   
System Test
------------
To characterize the system performance between transmitter/receiver, control over the MER is required at the receiver. This can be acheived by adjusting the gain of the transmitter, the gain of the receiver, or the distance between transmit/receive antennas. Adjusting the gain of the receiver is not desirable because the change in gain will not result in the same change in MER due to the nature of noise figure. Changing the antenna distance is also not a desirable approach as it becomes difficult to control the MER because of :math:`\frac{1}{R^2}` spreading, e.g. changing the antenna distance from 1 inch to 2 inches is not a 1dB change in the MER.

This leaves the desirable approach of adjusting the transmitter gain. Increasing/descreasing transmitter gain results in the same change in MER as well as a correlated change in the BER. A reference plot is shown below. In the reference plot, the QPSK data is not valid data because the software AGC in the model was limited, causing the MER to saturate. In a properly designed system, it is expected that the solid red and green curves continue to increase similar to the solid black and blue traces on the plot.

.. image:: pageImage/BER_MER_Data.png
   :width: 1200
   
MER Measurement - Challenges
-----------------------------
A custom MER function was designed and implemented to capture the average noise variance on the FPGA. The average symbol energy was assumed to be 1 based on observations seen in the simulation data. Although verified in simulation, the MER measurements, when deployed, was not reliable as moving the antennas further apart or reducing the transmitter/receiver gain has little effect on the measurement. Additionally, changing the Tx gain did not have an impact either. Both of these approaches did degrade the signal quality as messages were dropped when viewing the Simulink Console.

As alternatives to the original function, the following were tested:
- The custom function was modified to capture the average symbol energy on the hardware to mitigate the risk of an incorrect assumption. This led to the same results. 
- The raw symbols were streamed out an AXI DMA port in order to measure the MER. The DMA was previously used to steam out data bits. This was done to ensure the FPGA was not skewing numbers. This showed more promising results but requires an extra DMA to stream out both bits for data decoding and symbols for MER measurement simultaneously.

As an alternate design approach, to measure the channel quality, random symbols will be sent over the air on the back channel with the symbols being streamed out on the ARM for MER calculations. Although this method can provide a satisfactory result depending on the requirements for measurement accuracy, it is not the preferred one. It assumes that the channel channel characteristics between radio 1 to 2 on frequency A is the same going the opposite way on frequency B.

For future improvements to this project, it would be desirable to add Integrated Logic Analyzers (ILA's) into the design within Vivado and look at the data within the FPGA to isolate the issue. Addiitonaly, an FPGA with additional DMA stream ports available would allow the symbols to be streamed out in order to determine the MER on the ARM processor.