Packet Protocol
===================
.. image:: pageImage/packetProtocol.PNG
   :width: 800

An important part of the design is the packet protocol definition. Due to resource limitations on the Zedboard, mainly AXI stream ports, it is not feasible to design an independent transmitter and/or receiver for each waveform type (BPSK or QPSK). To circumvent this, the packet protocol is designed in such a way that information about the modulation type is embedded into the packet itself. 

**Advantages**

- Efficient use of DMA ports
- Ability to utilize the same Tx/Rx signal processing blocks for multiple modulation types
- All control logic is embedded within the packet itself
- No need for a dedicated pilot packet for channel estimation

**Disadvantages**

- Requires more complex demodulation logic
- Design is more sensitive to packet timing misalignment if not implemented properly

The packet includes a 13-bit Barker Code as a preamble used for synchronization. A Barker Code has good autocorrelation properties, which makes it desirable for this process. Following the preamble is a 41-bit guard interval that is used to prevent overflow from one part of the packet to another. Combined, the 54-bits are identical amongst all packet types and will be used as reference to characterize the noise variance introduced by the channel.

The next portion of the packet is an 8-bit modulation identifier that determines how the payload data is modulated and which error correction code rate used. The next 16-bit is a CRC used as a checksum for delivery verification. This is followed by an 8-bit Message ID that identifies the message type. The 54-bits previously described combined with these 3-bits will always be modulated BPSK with no error correction applied.

The next portion of the packet is the payload, which the user data sandwiched between additional guard bits, all with varying length depending on message type. The payload is the portion of the packet that will vary in modulation and coding.

A detailed breakdown of each message type is shown in the table below.

+-----------------------------+------------+------------+------------+------------+
| Parameters                  | QPSK (2/3) | QPSK (1/2) | BPSK (2/3) | BPSK (1/2) |
+=============================+============+============+============+============+
| Code Rate                   |    2/3     |    1/2     |    2/3     |    1/2     |
+-----------------------------+------------+------------+------------+------------+
| Mod Order (Header)          |     1      |     1      |     1      |     1      |
+-----------------------------+------------+------------+------------+------------+
| Mod Order (Message)         |     2      |     2      |     1      |     1      |
+-----------------------------+------------+------------+------------+------------+
| Upsample Rate               |     2      |     2      |     2      |     2      |
+-----------------------------+------------+------------+------------+------------+
| Puncture Length             |     4      |     6      |     4      |     6      |
+-----------------------------+------------+------------+------------+------------+
| Preamble                    |     13     |     13     |     13     |     13     |
+-----------------------------+------------+------------+------------+------------+
| Guard Bits 1                |     41     |     41     |     41     |     41     |
+-----------------------------+------------+------------+------------+------------+
| ModID/MsgID/CRC             |     32     |     32     |     32     |     32     |
+-----------------------------+------------+------------+------------+------------+
| Guard Bits 2                |     84     |     420    |     42     |     210    |
+-----------------------------+------------+------------+------------+------------+
| # of Messages               |     58     |     42     |     29     |     21     |
+-----------------------------+------------+------------+------------+------------+
| # of Message Bits           |     6496   |     4704   |     3248   |     2352   |
+-----------------------------+------------+------------+------------+------------+
| # of Message Bits (Coded)   |     9744   |     9408   |     4872   |     4704   |
+-----------------------------+------------+------------+------------+------------+
| # of Bits in Full Frame     |     9914   |     9914   |     5000   |    5000    |
+-----------------------------+------------+------------+------------+------------+
| # of Bit Pairs              |     5000   |     5000   |     5000   |     5000   |
+-----------------------------+------------+------------+------------+------------+
| # of Symbols                |     5000   |     5000   |     5000   |     5000   |
+-----------------------------+------------+------------+------------+------------+
| # of Symbols (Upsampled)    |     10000  |     10000  |     10000  |     10000  |
+-----------------------------+------------+------------+------------+------------+