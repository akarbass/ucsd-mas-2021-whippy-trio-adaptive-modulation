.. Adaptive Modulation documentation master file, created by
   sphinx-quickstart on Fri Jun  4 16:18:00 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. _settingup:

UCSD MAS 2021 Adaptive Modulation
===============================================

.. toctree::
   :maxdepth: 1
   :caption: Contents
   
   pages/Background Info
   pages/Simulink Design
   pages/Packet Protocol
   pages/MER

Project Overview
--------------------------

.. raw:: html

   <div style="position: relative; padding-bottom: 10.00%%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
       <iframe width="560" height="315" src="https://www.youtube.com/embed/i6Ct0WcKqUE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>


For this project, the objective is to design, implement, and test a wireless communication system that adjusts the modulation and coding scheme (MCS) based on the conditions of the channel. The modulation schemes for this design are BPSK and QPSK. Both of the modulation schemes will be paired with a Convolutional Encoder/Viterbi Decoder that is used for error correcting. The combinations result in modulation/code rate of:

- BPSK with Code Rate = 1/2
- BPSK with Code Rate = 2/3
- QPSK with Code Rate = 1/2
- QPSK with Code Rate = 2/3 

The entire design will be simulated and deployed using Matlab & Simulink, with the design being synthesized to hardware using Xilinx Vivado Design Suite.

System Description
---------------------


Hardware
~~~~~~~~~~~
| AVNET Zedboard Zynq-7000
| Analog Devices FMCOMMS4

Design Tools
~~~~~~~~~~~~~~
| Mathworks MATLAB & Simulink
| Xilinx Vivado Design Suite

System Overview
----------------- 

.. image:: images/systemblock.PNG
   :width: 600

The system comprises of two Zedboard/FMCOMMS4 transceivers. Let's call them radio A and radio B.  Both radios will be transmitting and receiving data.

The link from radio A Tx to radio B receive is the main channel where the user data is transmitted. On radio A transmitter, the modulation ID, the checksum, and message ID is embedded into the packet, along with the user data, so it can be used by the receiver to decode and print the proper messages. Additional information about how the message is constructed can be found under the Packet Protocol Section.

The link from radio B to radio A is considered the backchannel. This is where the pilot packet is sent in order to measure the performance of the channel. This channel frequency has a 1 MHz offset from the main channel to avoid interference.



